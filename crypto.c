
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <memory.h>

#include "common.h"
#include "crypto.h"

static char super_secret_key[] =
"-----BEGIN RSA PRIVATE KEY-----\n"
"MIIEpQIBAAKCAQEA59dE8qLieItsH1WgjrcFRKj6eUWqi+bGLOX1HL3U3GhC/j0Qg90u3sG/1CUt\n"
"wC5vOYvfDmFI6oSFXi5ELabWJmT2dKHzBJKa3k9ok+8t9ucRqMd6DZHJ2YCCLlDRKSKv6kDqnw4U\n"
"wPdpOMXziC/AMj3Z/lUVX1G7WSHCAWKf1zNS1eLvqr+boEjXuBOitnZ/bDzPHrTOZz0Dew0uowxf\n"
"/+sG+NCK3eQJVxqcaJ/vEHKIVd2M+5qL71yJQ+87X6oV3eaYvt3zWZYD6z5vYTcrtij2VZ9Zmni/\n"
"UAaHqn9JdsBWLUEpVviYnhimNVvYFZeCXg/IdTQ+x4IRdiXNv5hEewIDAQABAoIBAQDl8Axy9XfW\n"
"BLmkzkEiqoSwF0PsmVrPzH9KsnwLGH+QZlvjWd8SWYGN7u1507HvhF5N3drJoVU3O14nDY4TFQAa\n"
"LlJ9VM35AApXaLyY1ERrN7u9ALKd2LUwYhM7Km539O4yUFYikE2nIPscEsA5ltpxOgUGCY7b7ez5\n"
"NtD6nL1ZKauw7aNXmVAvmJTcuPxWmoktF3gDJKK2wxZuNGcJE0uFQEG4Z3BrWP7yoNuSK3dii2jm\n"
"lpPHr0O/KnPQtzI3eguhe0TwUem/eYSdyzMyVx/YpwkzwtYL3sR5k0o9rKQLtvLzfAqdBxBurciz\n"
"aaA/L0HIgAmOit1GJA2saMxTVPNhAoGBAPfgv1oeZxgxmotiCcMXFEQEWflzhWYTsXrhUIuz5jFu\n"
"a39GLS99ZEErhLdrwj8rDDViRVJ5skOp9zFvlYAHs0xh92ji1E7V/ysnKBfsMrPkk5KSKPrnjndM\n"
"oPdevWnVkgJ5jxFuNgxkOLMuG9i53B4yMvDTCRiIPMQ++N2iLDaRAoGBAO9v//mU8eVkQaoANf0Z\n"
"oMjW8CN4xwWA2cSEIHkd9AfFkftuv8oyLDCG3ZAf0vrhrrtkrfa7ef+AUb69DNggq4mHQAYBp7L+\n"
"k5DKzJrKuO0r+R0YbY9pZD1+/g9dVt91d6LQNepUE/yY2PP5CNoFmjedpLHMOPFdVgqDzDFxU8hL\n"
"AoGBANDrr7xAJbqBjHVwIzQ4To9pb4BNeqDndk5Qe7fT3+/H1njGaC0/rXE0Qb7q5ySgnsCb3DvA\n"
"cJyRM9SJ7OKlGt0FMSdJD5KG0XPIpAVNwgpXXH5MDJg09KHeh0kXo+QA6viFBi21y340NonnEfdf\n"
"54PX4ZGS/Xac1UK+pLkBB+zRAoGAf0AY3H3qKS2lMEI4bzEFoHeK3G895pDaK3TFBVmD7fV0Zhov\n"
"17fegFPMwOII8MisYm9ZfT2Z0s5Ro3s5rkt+nvLAdfC/PYPKzTLalpGSwomSNYJcB9HNMlmhkGzc\n"
"1JnLYT4iyUyx6pcZBmCd8bD0iwY/FzcgNDaUmbX9+XDvRA0CgYEAkE7pIPlE71qvfJQgoA9em0gI\n"
"LAuE4Pu13aKiJnfft7hIjbK+5kyb3TysZvoyDnb3HOKvInK7vXbKuU4ISgxB2bB3HcYzQMGsz1qJ\n"
"2gG0N5hvJpzwwhbhXqFKA4zaaSrw622wDniAK5MlIE0tIAKKP4yxNGjoD2QYjhBGuhvkWKY=\n"
"-----END RSA PRIVATE KEY-----";

#ifdef CONFIG_HAVE_OPENSSL
#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <openssl/evp.h>
#include <openssl/bio.h>
#include <openssl/buffer.h>

static RSA *rsa = NULL;

char *base64_enc(uint8_t *input, int length) {
    BIO *bmem, *b64;
    BUF_MEM *bptr;
    b64 = BIO_new(BIO_f_base64());
    bmem = BIO_new(BIO_s_mem());
    b64 = BIO_push(b64, bmem);
    BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);
    BIO_write(b64, input, length);
    BIO_flush(b64);
    BIO_get_mem_ptr(b64, &bptr);

    char *buf = (char *)malloc(bptr->length);
    if (bptr->length) {
        memcpy(buf, bptr->data, bptr->length-1);
        buf[bptr->length-1] = 0;
    }

    BIO_free_all(bmem);

    return buf;
}

uint8_t *base64_dec(char *input, int *outlen) {
    BIO *bmem, *b64;
    int inlen = strlen(input);

    b64 = BIO_new(BIO_f_base64());
    BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);
    bmem = BIO_new(BIO_s_mem());
    b64 = BIO_push(b64, bmem);

    // Apple cut the padding off their challenges; restore it
    BIO_write(bmem, input, inlen);
    while (inlen++ & 3)
        BIO_write(bmem, "=", 1);
    BIO_flush(bmem);

    int bufsize = strlen(input)*3/4 + 1;
    uint8_t *buf = malloc(bufsize);
    int nread;

    nread = BIO_read(b64, buf, bufsize);

    BIO_free_all(bmem);

    *outlen = nread;
    return buf;
}

uint8_t *rsa_apply(uint8_t *input, int inlen, int *outlen, int mode) {

    if (!rsa) {
        BIO *bmem = BIO_new_mem_buf(super_secret_key, -1);
        rsa = PEM_read_bio_RSAPrivateKey(bmem, NULL, NULL, NULL);
        BIO_free(bmem);
    }
    uint8_t *out = malloc(RSA_size(rsa));
    switch (mode) {
        case RSA_MODE_AUTH:
            *outlen = RSA_private_encrypt(inlen, input, out, rsa, RSA_PKCS1_PADDING);
            break;
        case RSA_MODE_KEY:
            *outlen = RSA_private_decrypt(inlen, input, out, rsa, RSA_PKCS1_OAEP_PADDING);
            break;
        default:
            die("bad rsa mode");
    }
    return out;
}

#endif // CONFIG_HAVE_OPENSSL


/************************************************************/
/*                                                          */
/************************************************************/

#ifdef CONFIG_HAVE_POLARSSL
#include <polarssl/rsa.h>
#include <polarssl/base64.h>
#include <polarssl/pk.h>
#include <polarssl/entropy.h>
#include <polarssl/ctr_drbg.h>

typedef struct pl_rsa_s{
    entropy_context entropy;
    ctr_drbg_context ctr_drbg;
    rsa_context rsa;
}pl_rsa_t;

static pl_rsa_t __rsa_auth;
static pl_rsa_t __rsa_key;
static pl_rsa_t *rsa_auth = NULL;
static pl_rsa_t *rsa_key = NULL;

static int pl_rsa_copy(pl_rsa_t *key, pk_context *pk, int padding_mode, int padding_type){    
    if(rsa_copy(&key->rsa, pk_rsa(*pk))){
        fprintf(stderr, "%s\n", "rsa_copy key error");
    }
    entropy_init(&key->entropy);
    ctr_drbg_init(&key->ctr_drbg, entropy_func, &key->entropy, NULL, 0);
    rsa_set_padding(&key->rsa, padding_mode, padding_type);
    return 0;
}

static int pl_RSA_private_encrypt(pl_rsa_t* key, int size, unsigned char* input, unsigned char* output){
    if(!rsa_pkcs1_encrypt(&key->rsa, ctr_drbg_random, &key->ctr_drbg, RSA_PRIVATE, size, input, output))
        return key->rsa.len;
    return -1; 
}

static int pl_RSA_private_decrypt(pl_rsa_t* key, int size, unsigned char* input, unsigned char* output){
    size_t outsize = (size_t)key->rsa.len;
    int ret = rsa_pkcs1_decrypt(&key->rsa, ctr_drbg_random, &key->ctr_drbg, RSA_PRIVATE, &outsize, input, output, outsize);
    if(!ret)
        return (int)outsize;
    return -1; 
}

char *base64_enc(uint8_t *input, int length) {
    size_t require_size = 0;
    char *buf = NULL;
    base64_encode(NULL, &require_size, input, length);
    buf = (char*)malloc(require_size);
    if(base64_encode((unsigned char*)buf, &require_size, input, length)){
        free(buf);
        buf = NULL;
    }
    return buf;
}

uint8_t *base64_dec(char *input, int *outlen) {
    int input_len = strlen(input);
    size_t padding_len = (input_len+3)/4*4;
    char* padding_buff = NULL;
    size_t dec_size = padding_len+1;
    uint8_t *dec_buf = NULL;
    if(padding_len != input_len){
        padding_buff = malloc(padding_len);
        memcpy(padding_buff, input, input_len);
        memset(padding_buff+input_len, '=', padding_len-input_len);
        fprintf(stderr, "%s\n", "padding_len != input_len");
    }else{
        padding_buff = input;
    }

    dec_buf = malloc(dec_size);

    if(base64_decode((unsigned char*)dec_buf, &dec_size, (const unsigned char*)padding_buff, padding_len)){
        free(dec_buf);
        dec_buf = NULL;
    }

    if(padding_len != input_len){
        free(padding_buff);
    }

    *outlen = dec_size;
    return dec_buf;
}

uint8_t *rsa_apply(uint8_t *input, int inlen, int *outlen, int mode) {
    if(!rsa_auth || !rsa_key){
        pk_context pk;
        pk_init(&pk);
        if(pk_parse_key(&pk, (const unsigned char*)super_secret_key, strlen(super_secret_key), NULL, 0)){
            fprintf(stderr, "%s\n", "pk_parse_key error");
        }

        rsa_auth = &__rsa_auth;        
        pl_rsa_copy(rsa_auth, &pk, RSA_PKCS_V15, POLARSSL_MD_SHA1);

        rsa_key = &__rsa_key;
        pl_rsa_copy(rsa_key, &pk, RSA_PKCS_V21, POLARSSL_MD_SHA1);

        pk_free(&pk);
    }

    uint8_t *out = malloc(rsa_key->rsa.len+1);
    switch (mode) {
        case RSA_MODE_AUTH:
            *outlen = pl_RSA_private_encrypt(rsa_auth, inlen, input, out);
            break;
        case RSA_MODE_KEY:
            *outlen = pl_RSA_private_decrypt(rsa_key, inlen, input, out);
            break;
        default:
            die("bad rsa mode");
    }
    return out;
}

#endif // CONFIG_HAVE_POLARSSL