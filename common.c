/*
 * Utility routines. This file is part of Shairport.
 * Copyright (c) James Laird 2013
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <memory.h>
#include <unistd.h>
#include "common.h"
#include "daemon.h"

shairport_cfg config;

int debuglev = 0;

void die(char *format, ...) {
    fprintf(stderr, "FATAL: ");

    va_list args;
    va_start(args, format);

    vfprintf(stderr, format, args);
    if (config.daemonise)
        daemon_fail(format, args); // Send error message to parent

    va_end(args);

    fprintf(stderr, "\n");
    shairport_shutdown(1);
}

void warn(char *format, ...) {
    fprintf(stderr, "WARNING: ");
    va_list args;
    va_start(args, format);
    vfprintf(stderr, format, args);
    va_end(args);
    fprintf(stderr, "\n");
}

void debug(int level, char *format, ...) {
    if (level > debuglev)
        return;
    va_list args;
    va_start(args, format);
    vfprintf(stderr, format, args);
    va_end(args);
}

void command_start(void) {
    if (!config.cmd_start)
        return;
    if (!config.cmd_blocking && fork())
        return;

    debug(1, "running start command: %s", config.cmd_start);
    if (system(config.cmd_start))
        warn("exec of external start command failed");

    if (!config.cmd_blocking)
        exit(0);
}

void command_stop(void) {
    if (!config.cmd_stop)
        return;
    if (!config.cmd_blocking && fork())
        return;

    debug(1, "running stop command: %s", config.cmd_stop);
    if (system(config.cmd_stop))
        warn("exec of external stop command failed");

    if (!config.cmd_blocking)
        exit(0);
}
