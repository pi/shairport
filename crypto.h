#ifndef __crypto_h__
#define __crypto_h__
#include <stdint.h>

#ifdef CONFIG_HAVE_OPENSSL
#include <openssl/md5.h>
#include <openssl/aes.h>
#endif // CONFIG_HAVE_OPENSSL

#ifdef CONFIG_HAVE_POLARSSL

#include <polarssl/aes.h>
#include <polarssl/md5.h>

#define AES_SIZE                16
#define AES_BLOCK_SIZE          16
#define AES_KEY                 aes_context
#define MD5_CTX                 md5_context

#define MD5_Init( CTX ) \
        md5_starts( (CTX) )
#define MD5_Update( CTX, BUF, LEN ) \
        md5_update( (CTX), (unsigned char *)(BUF), (LEN) )
#define MD5_Final( OUT, CTX ) \
        md5_finish( (CTX), (OUT) )

#define AES_set_encrypt_key( KEY, KEYSIZE, CTX ) \
        aes_setkey_enc( (CTX), (KEY), (KEYSIZE) )
#define AES_set_decrypt_key( KEY, KEYSIZE, CTX ) \
        aes_setkey_dec( (CTX), (KEY), (KEYSIZE) )
#define AES_cbc_encrypt( INPUT, OUTPUT, LEN, CTX, IV, MODE ) \
        aes_crypt_cbc( (CTX), (MODE), (LEN), (IV), (INPUT), (OUTPUT) )

#endif // CONFIG_HAVE_POLARSSL

uint8_t *base64_dec(char *input, int *outlen);
char *base64_enc(uint8_t *input, int length);

#define RSA_MODE_AUTH (0)
#define RSA_MODE_KEY  (1)
uint8_t *rsa_apply(uint8_t *input, int inlen, int *outlen, int mode);


#endif